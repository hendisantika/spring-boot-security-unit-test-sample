package com.hendisantika.security.springbootsecurityunittestsample.exception;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-security-unit-test-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-07
 * Time: 12:41
 * To change this template use File | Settings | File Templates.
 */
public class InvalidUserIdException extends ValidationException {

    public InvalidUserIdException(String message) {
        super(message);
    }
}