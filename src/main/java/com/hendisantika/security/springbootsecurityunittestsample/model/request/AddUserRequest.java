package com.hendisantika.security.springbootsecurityunittestsample.model.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-security-unit-test-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-07
 * Time: 12:02
 * To change this template use File | Settings | File Templates.
 */
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@ApiModel(value = "User")
public class AddUserRequest implements Serializable {

    @NotNull(message = "Name is required")
    @ApiModelProperty(notes = "Name of the user")
    private String name;

    @NotNull(message = "Username is required")
    @ApiModelProperty(notes = "Username of the user")
    private String username;

    @NotNull(message = "Password is required")
    @ApiModelProperty(notes = "Password of the user")
    private String password;
}