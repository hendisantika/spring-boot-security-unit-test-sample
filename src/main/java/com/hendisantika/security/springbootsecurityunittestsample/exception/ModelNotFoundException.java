package com.hendisantika.security.springbootsecurityunittestsample.exception;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-security-unit-test-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-07
 * Time: 12:42
 * To change this template use File | Settings | File Templates.
 */
public class ModelNotFoundException extends AppGenericException {

    public ModelNotFoundException(String message) {
        super(message);
    }
}