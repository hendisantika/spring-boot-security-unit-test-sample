package com.hendisantika.security.springbootsecurityunittestsample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootSecurityUnitTestSampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootSecurityUnitTestSampleApplication.class, args);
    }

}

