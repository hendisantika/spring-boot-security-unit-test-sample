package com.hendisantika.security.springbootsecurityunittestsample.service;

import com.hendisantika.security.springbootsecurityunittestsample.exception.ModelNotFoundException;
import com.hendisantika.security.springbootsecurityunittestsample.exception.PersistentException;
import com.hendisantika.security.springbootsecurityunittestsample.model.AddUserResponse;
import com.hendisantika.security.springbootsecurityunittestsample.model.FindUserResponse;
import com.hendisantika.security.springbootsecurityunittestsample.model.request.AddUserRequest;
import org.springframework.stereotype.Service;

import javax.annotation.security.RolesAllowed;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-security-unit-test-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-07
 * Time: 12:43
 * To change this template use File | Settings | File Templates.
 */
@Service
public class UserService implements GenericService<AddUserRequest, AddUserResponse> {

    @Override
    @RolesAllowed("ROLE_ADMIN")
    public AddUserResponse create(AddUserRequest addUserRequest) throws PersistentException {

        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        AddUserResponse addUserResponse = new AddUserResponse();
        addUserResponse.setUserId(1);
        addUserResponse.setUsername(addUserRequest.getUsername());
        addUserResponse.setCreatedOn(sdf.format(cal.getTime()));
        return addUserResponse;
    }


    @RolesAllowed("ROLE_USER")
    public FindUserResponse findUserById(Integer id) throws ModelNotFoundException {
        if (id > 100) {
            throw new ModelNotFoundException("Valid user id is required ");
        }
        return FindUserResponse.builder()
                .userId(id)
                .name("Uzumaki Naruto")
                .username("narutp")
                .build();
    }
}
