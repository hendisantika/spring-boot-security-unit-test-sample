package com.hendisantika.security.springbootsecurityunittestsample.controller;

import com.hendisantika.security.springbootsecurityunittestsample.exception.InvalidUserIdException;
import com.hendisantika.security.springbootsecurityunittestsample.exception.ModelNotFoundException;
import com.hendisantika.security.springbootsecurityunittestsample.exception.PersistentException;
import com.hendisantika.security.springbootsecurityunittestsample.exception.ValidationException;
import com.hendisantika.security.springbootsecurityunittestsample.model.AddUserResponse;
import com.hendisantika.security.springbootsecurityunittestsample.model.FindUserResponse;
import com.hendisantika.security.springbootsecurityunittestsample.model.request.AddUserRequest;
import com.hendisantika.security.springbootsecurityunittestsample.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-security-unit-test-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-07
 * Time: 21:06
 * To change this template use File | Settings | File Templates.
 */
@RestController
@Api(value = "user-controller", description = "Endpoints for handling and managing user related operations", tags = "/users")
public class UserController {

    @Autowired
    private UserService userService;

    @ApiOperation(value = "Creating new user")
    @PostMapping("/users")
    public AddUserResponse createUser(@Valid @RequestBody AddUserRequest addUserRequest, BindingResult bindingResult) throws PersistentException, ValidationException {
        if (bindingResult.hasErrors()) {
            throw new ValidationException(bindingResult.getAllErrors().get(0).getDefaultMessage());
        }
        return userService.create(addUserRequest);
    }


    @ApiOperation(value = "finding the user by id")
    @GetMapping("/users/{id}")
    public FindUserResponse findUserById(@PathVariable("id") Integer id) throws ModelNotFoundException, InvalidUserIdException {
        if (id > 0) {
            return userService.findUserById(id);
        }
        throw new InvalidUserIdException("Invalid  user Id");
    }
}