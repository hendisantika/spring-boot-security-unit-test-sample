package com.hendisantika.security.springbootsecurityunittestsample.controller;

import com.hendisantika.security.springbootsecurityunittestsample.exception.InvalidUserIdException;
import com.hendisantika.security.springbootsecurityunittestsample.exception.ModelNotFoundException;
import com.hendisantika.security.springbootsecurityunittestsample.exception.PersistentException;
import com.hendisantika.security.springbootsecurityunittestsample.exception.ValidationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-security-unit-test-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-07
 * Time: 21:06
 * To change this template use File | Settings | File Templates.
 */
@RestControllerAdvice
public class AppRestControllerAdvice {

    private final static String MESSAGE = "message";


    @ExceptionHandler(value = {ModelNotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Map<String, String> handleModelNotFoundException(Exception ex) {
        Map<String, String> errorMap = new HashMap<>();
        errorMap.put(MESSAGE, ex.getMessage());
        return errorMap;
    }


    @ExceptionHandler(value = {ValidationException.class, InvalidUserIdException.class, PersistentException.class})
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    public Map<String, String> handleValidationRelatedExceptions(Exception ex) {
        Map<String, String> errorMap = new HashMap<>();
        errorMap.put(MESSAGE, ex.getMessage());
        return errorMap;
    }
}