package com.hendisantika.security.springbootsecurityunittestsample.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-security-unit-test-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-07
 * Time: 12:00
 * To change this template use File | Settings | File Templates.
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
public class AddUserResponse implements Serializable {

    @JsonProperty("user_id")
    private Integer userId;

    private String username;

    @JsonProperty("created_on")
    private String createdOn;
}