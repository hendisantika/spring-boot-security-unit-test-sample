package com.hendisantika.security.springbootsecurityunittestsample.service;

import com.hendisantika.security.springbootsecurityunittestsample.exception.PersistentException;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-security-unit-test-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-07
 * Time: 12:39
 * To change this template use File | Settings | File Templates.
 */
public interface GenericService<Q, S> {

    S create(Q rq) throws PersistentException;
}