package com.hendisantika.security.springbootsecurityunittestsample.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-security-unit-test-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-07
 * Time: 12:01
 * To change this template use File | Settings | File Templates.
 */
@Builder
@AllArgsConstructor
@Getter(AccessLevel.PUBLIC)
public class FindUserResponse implements Serializable {
    @JsonProperty("user_id")
    private Integer userId;
    private String name;
    private String username;
}